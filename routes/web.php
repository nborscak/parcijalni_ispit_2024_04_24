<?php

use App\Http\Controllers\BlogController;
use Illuminate\Support\Facades\Route;
use \App\Http\Controllers\UserController;
use \App\Http\Controllers\OmeniController;


Route::get('/', function () {return view('homepage');});
Route::get('/omeni', [OmeniController::class, 'index']);
Route::get('/postovi', [BlogController::class, 'index']);

Route::get('/registration', function () {return view('registration');});
Route::get('/login', function () {return view('login');});
Route::get('/logout', [UserController::class, 'logout']);

Route::post('/registration', [UserController::class, 'registration']);
Route::post('/login', [UserController::class, 'login']);

Route::group(['middleware' => ['auth:sanctum']], function()
{
    Route::put('/omeni', [OmeniController::class, 'update']);
    Route::post('/logout', [UserController::class, 'logout']);

    Route::get('/create', [BlogController::class, 'create']);
    Route::post('/create', [BlogController::class, 'store']);
    Route::get('/update/{id}', [BlogController::class, 'update']);
    Route::put('/update/{id}', [BlogController::class, 'edit']);
    Route::get('/ja/{id}', [OmeniController::class, 'update']);
    Route::put('/ja/{id}', [OmeniController::class, 'edit']);
    Route::post('/store', [BlogController::class, 'store']);

    Route::delete('/postovi/{id}', [BlogController::class, 'destroy']);

});

