<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Omeni;

class OmeniController extends Controller
{
    public function index()
    {
        return view('omeni',
        [
            'ja' => Omeni::all()
        ]);
        
    }

    public function update($id)
    {
        $omeni = Omeni::find($id);

        return view('ja',
        [
            'id' => $omeni
        ]);
    }

    public function edit(Request $request, $id)
    {
        $omeni= Omeni::find($id);

        $omeni['name'] = $request->name;
        $omeni['detalji'] = $request->detalji;

        $omeni->save();

        return redirect('/omeni');

    }



}
