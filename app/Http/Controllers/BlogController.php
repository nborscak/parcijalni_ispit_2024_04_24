<?php

namespace App\Http\Controllers;

use App\Models\Blog;
use Illuminate\Http\Request;

class BlogController extends Controller
{
    public function index()
    {
        return view('postovi',
        [
            'post' => Blog::all()
        ]);
    }

    public function store(Request $request)
    {
        $formfields = $request->validate(
            [
                'naslov' => 'required',
                'post' => 'required'
            ]
            );

        Blog::create($formfields);

        return redirect('/postovi');


    }

    public function create()
    {
        return view('create');
    }

    public function update($id)
    {
        $post = Blog::find($id);

        return view('update',
        [
            'post' => $post
        ]);
    }

    public function edit(Request $request, $id)
    {
        $post= Blog::find($id);

        $post['naslov'] = $request->naslov;
        $post['post'] = $request->post;

        $post->save();

        return redirect('/postovi');

        // if (!isset($post))
        // {
        //     return response()->json(['error' => 'Post nije pronađen'], 404);
        // }

        // if (isset($request->naslov) && $request->naslov != '')
        // {
        //     $post->naslov= $request->naslov;
        // }

        // if (isset($request->post) && $request->post != '')
        // {
        //     $post->post = $request->post;
        // }

        // $post->save();

        // return redirect('/postovi');

    }

    public function destroy($id)
    {
        Blog::find($id)->delete();

        return redirect('/postovi');
    }
}
