<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Azuriraj opis o meni</h2>
        </div>
        <form method="POST" action="/ja/{{$omeni->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naslov</label>
              <input type="text" name="name" value="{{$omeni->name}}" class="form-control" id="exampleInputName" aria-describedby="nameHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Opis</label>
                <input type="text" name="detalji" value="{{$omeni->detalji}}" class="form-control" id="exampleInputYear" aria-describedby="OpisHelp">
            </div>
            <button type="submit" class="btn btn-primary">Spremi</button>
        </form>
    </div>
</x-layout>