<x-layout>
    <style>
        button {
           background-color: rgb(153, 137, 91);
           color: rgb(201, 201, 191);
          /* font-style: italic; */
          font-family: Arial, Helvetica, sans-serif;
          font-size: 2em;
          border-radius: 10px; 
            border: none; 
            padding: 10px 20px; 
            
        }
        
        button a {
            text-decoration: none; 
            color: inherit; 
        }
     
      </style>
    <div class="row">
        <div class="mb-3">
            <h2>Login</h2>
        </div>
        <form method="POST" action="/login">
            @csrf
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Email</label>
                <input type="email" name="email" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Lozinka</label>
                <input type="password" name="password" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
            </div>
            <button type="submit" class="btn btn-primary">Prijava</button>
        </form>
    </div>
</x-layout>