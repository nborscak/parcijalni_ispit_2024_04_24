<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Unesi novi post</h2>
        </div>
        <form method="POST" action="/create">
            @csrf
            <div class="mb-3">
              <label for="exampleInputEmail" class="form-label">Naslov</label>
              <input type="text" name="naslov" class="form-control" id="exampleInputNaslov" aria-describedby="naslovHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail" class="form-label">Post</label>
                <input type="text" name="post" class="form-control" id="exampleInputPost" aria-describedby="postHelp">
            </div>
            <button type="submit" class="btn btn-primary">Postaj</button>
        </form>
    </div>
</x-layout>