<!doctype html>
<html lang="en">
  <head>
    <!-- Required meta tags -->
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- Bootstrap CSS -->
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-EVSTQN3/azprG1Anm3QDgpJLIm9Nao0Yz1ztcQTwFspd3yD65VohhpuuCOmLASjC" crossorigin="anonymous">

    <title>Moj blog</title>
  
    <style>
      body {
        background-color: rgb(245, 221, 176);
        color: rgb(122, 122, 119);
        /* font-style: italic; */
        font-family: Arial, Helvetica, sans-serif;
        padding: 5%
      }
       

    </style>
  </head>
  <body>
    <div>
      <h1>~Dobrodošli na moj blog~</h1>
      <p>______________________________________________</p>
    </div>

    <nav class="navbar navbar-expand-lg navbar-light bg-light">
      <div class="container-fluid">
        <a class="navbar-brand" href="/">Home</a>
        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
          <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
          <ul class="navbar-nav me-auto mb-2 mb-lg-0">
            @auth
            <li class="nav-item">
              <form method="POST" action="/logout">
                @csrf
                <button class="btn btn-light" type="submit">Odjavi se</a>
              </form>
            </li>
            @else
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="/registration">  
              Registriraj se</a>
            </li>
            <li class="nav-item">
              <a class="nav-link" aria-current="page" href="/login">Login</a>
            </li>
            @endauth
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="/omeni">  
              O meni</a>
            </li>
            <li class="nav-item">
              <a class="nav-link active" aria-current="page" href="/postovi">  
              Blog</a>
            </li>
          </ul>
        </div>
      </div>
    </nav>
  </br>
    

    <div class="container">
      {{$slot}}
    </div>

    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.0.2/dist/js/bootstrap.bundle.min.js" integrity="sha384-MrcW6ZMFYlzcLA8Nl+NtUVF0sA7MsXsP1UyJoMp4YLEuNSfAP+JcXn/tWtIaxVXM" crossorigin="anonymous"></script>

  </body>
</html>
