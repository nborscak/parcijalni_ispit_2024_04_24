<x-layout>
    <div class="row">
        <div class="mb-3">
            <h2>Azuriraj post</h2>
        </div>
        <form method="POST" action="/postovi/{{$post->id}}">
            @csrf
            @method("PUT")
            <div class="mb-3">
              <label for="exampleInputEmail1" class="form-label">Naslov</label>
              <input type="text" name="naslov" value="{{$post->naslov}}" class="form-control" id="exampleInputName" aria-describedby="nameHelp">
            </div>
            <div class="mb-3">
                <label for="exampleInputEmail1" class="form-label">Post</label>
                <input type="text" name="post" value="{{$post->post}}" class="form-control" id="exampleInputYear" aria-describedby="nameYear">
            </div>
            <button type="submit" class="btn btn-primary">Spremi</button>
        </form>
    </div>
</x-layout>