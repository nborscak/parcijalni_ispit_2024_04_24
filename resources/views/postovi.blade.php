<x-layout>
  <!DOCTYPE html>
  <html lang="hr">
  <head>
  <meta charset="UTF-8">
  <meta name="viewport" content="width=device-width, initial-scale=1.0">
  <title>Blog postovi</title>
  
  <style>
   
    body {
      background-color: rgb(245, 221, 176);
      color: rgb(122, 122, 119);
      /* font-style: italic; */
      font-family: Arial, Helvetica, sans-serif;
    }
  </style>

    </head>
    <body>
    @auth  
         <div class="row">
             <div class="col-sm-2">
                 <a class="btn btn-primary" href="/create">Dodaj novi post</a>
             </div>
         </div>
    @endauth
      <main>
        <table class="table" style="all: inherit">
          <tbody>
            <thead>
              <tr>
                <th scope="col">#</th>
                <th scope="col">Naslov</th>
                <th scope="col">Post</th>
                <th scope="col">Kreiran</th>
                <th scope="col">Ažuriran</th>
                @auth
                <th scope="col">Ažuriran</th>
                @endauth
              </tr>
            </thead>

            @foreach ($post as $key=>$posts)
          
            <tr>
              <th scope="row">{{($key + 1)}}</th>
              
                <td>{{$posts->naslov}}</td>
                <td>{{$posts->post}}</td>
                <td>{{$posts->created_at}}</td>
                <td>{{$posts->updated_at}}</td>
                @auth
                <td>
                    <a class="btn btn-secondary" href="/update/{{$posts->id}}">Uredi post</a>
                    <form method="POST" action="/postovi/{{$posts->id}}">
                        @csrf
                        @method('DELETE')
                        <button class="btn btn-danger" type="submit">Izbrisi post</a>
                    </form>
                </td>
                @endauth
            </tr>
            @endforeach
      
          </tbody>
        </table>
      
        <footer>
          <p><i>&copy; 2024 Nataša Boršćak. Sva prava pridržana.</i></p>
        </footer>
      

    </body>
    </html>
</x-layout>