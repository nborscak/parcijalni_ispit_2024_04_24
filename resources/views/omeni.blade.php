<x-layout>
<!DOCTYPE html>
<html lang="hr">
<head>
<meta charset="UTF-8">
<meta name="viewport" content="width=device-width, initial-scale=1.0">
<title>Dobrodošli u moj svijet</title>
</head>
<style>
 
  body {
    background-color: rgb(245, 221, 176);
    color: rgb(122, 122, 119);
    /* font-style: italic; */
    font-family: Arial, Helvetica, sans-serif;
  }
</style>

<body>

<header>
  <h1>Dobrodošli u moj svijet</h1>
</header>

<main>
  <table class="table" style="all: inherit">
    <tbody>
      <thead>
        <tr>
          <th scope="col">#</th>
          <th scope="col">O meni</th>
          <th scope="col">Kreiran</th>
          <th scope="col">Ažuriran</th>
        </tr>
      </thead>

      @foreach ($ja as $key=>$omeni)
    
      <tr>
        <th scope="row">{{($key + 1)}}</th>
        
          <td>{{$omeni->detalji}}</td>
          <td>{{$omeni->created_at}}</td>
          <td>{{$omeni->updated_at}}</td>
    
      </tr>
      @endforeach
      @auth  
        <div class="row">
            <div class="col-sm-2">
              <a class="btn btn-secondary" href="/ja/{{$omeni->detalji}}">Uredi text</a>
            </div>
        </div>
      @endauth
    </tbody>
  </table>
</main>

<footer>
  <p><i>&copy; 2024 Nataša Boršćak. Sva prava pridržana.</i></p>
</footer>

</body>
 
</html>
</x-layout>